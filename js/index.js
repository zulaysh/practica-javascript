import hamburgerMenu from "./menu.js";
import { digitalClock } from "./reloj.js";
import { alarm } from "./reloj.js";
import { shortCuts,moveBall } from "./teclado.js";
import countdown from "./cuenta_regresiva.js";
import scrollTopButton from "./boton_scroll.js";
import darkTheme from "./tema_oscuro.js";
import responsiveMedia from "./objeto_responsive.js";
import searchFilters from "./filtro_busqueda.js";
import draw from "./sorteo.js";
import slider from "./carrusel.js";
import contactFormValidations from "./validaciones_formulario.js";


const d=document;
d.addEventListener("DOMContentLoaded",e=>{
    hamburgerMenu(".panel-btn", ".panel", ".menu a");
    digitalClock("#reloj","#activar-reloj", "#desactivar-reloj");
    alarm("assets/alarma.mp3","#activar-alarma", "#desactivar-alarma");
    countdown("countdown","november 04,2022 00:00:00", "Feliz cumpleaños");
    scrollTopButton(".scroll-top-btn");
    responsiveMedia("youtube","(min-width:1024px)",`<a href="https://www.youtube.com/watch?v=2BmMWIa7Qfg" target="_blank" rel="noopener">Ver video</a>`, `<iframe width="560" height="315" src="https://www.youtube.com/embed/2BmMWIa7Qfg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`);
    responsiveMedia("gmaps","(min-width:1024px)","Contenido Móvil", "Contenido Escritorio");
   searchFilters(".card-filter",".card");
   draw("#winner-btn",".player");
   slider();
   contactFormValidations();
   
})
d.addEventListener("keydown",(e)=>{
    shortCuts(e);
    moveBall(e,".ball",".stage");
})

darkTheme(".dark-theme-btn", "dark-mode");